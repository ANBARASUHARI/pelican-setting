AUTHOR = 'Anbarasu E'
SITENAME = 'Indian Rice'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'UTC'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('Opensource', 'https://opensource.com/article/19/1/getting-started-pelican'),)

# Social widget
SOCIAL = (('Twitter(#AnbarasuHari)','https://twitter.com/AnbarasuHari'),)

DEFAULT_PAGINATION = 2


# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True