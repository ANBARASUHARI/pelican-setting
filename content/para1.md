Title: Indian Rice Variety
Date: 30-08-2023 10:00
Category: Article
Tags: Health,News
Authors: Ranjit Devraj
Image: indian-rice-farmer.jpg
Summary: Indian rice variety shows promise for people with diabetes

 
[NEW DELHI] A scented rice variety grown in India’s remote northeast, known as Joha rice, not only prevents type 2 diabetes but is also rich in unsaturated fatty acids, which work against heart disease, scientists have found.

Diabetes is a major global health problem, affecting an estimated 537 million adults aged 20 to 79 years in 2021, according to the International Diabetes Federation. The figure is predicted to rise to 783 million by 2045.

Early onset of type 2 diabetes is increasingly common but the condition is reversible through changes in lifestyles and diet, including the moderation of white rice consumption. Rice is a staple in many countries but it can affect blood sugar levels and increase the risk of diabetes.