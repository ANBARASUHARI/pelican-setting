Title: Ttobacco Farming
Date: 30-08-2023 12:00
Category: Article
Tags: Farming,News
Authors: Sanjeet Bagcchi
Image: Tobacco_fields.jpg
Summary: WHO calls on Asian countries to quit tobacco farming

[NEW DELHI] The World Health Organization (WHO) has asked countries in its Western Pacific region to repeal subsidies on growing tobacco and, instead, promote the cultivation of sustainable crops, including rice and vegetables.

About 400 million people in the Western Pacific region are smokers and three million a year die from tobacco-related diseases, WHO figures show.