Title: Nanotech System
Date: 30-08-2023 11:30
Category: Article
Tags: Agriculture,News
Authors: K.S. Neeraj
Image: rice_farming.jpg
Summary: Nanotech system detects toxic herbicide picloram

 
[NEW DELHI] Biochemical researchers say they have developed a novel system for the precise detection of the ultrasensitive herbicide picloram in rice water and soil samples.

A study published June in the journal Scientific Reports says the system is simple, rapid, cost-effective and highly accurate in detecting the substance, which can cause a number of health problems.

Picloram is a chlorinated derivative of picolinic acid which is sprayed on foliage, injected into plants, applied to cut surfaces, or allowed to leach to the roots. Once absorbed at any of these points, the herbicide is transported throughout the plant.